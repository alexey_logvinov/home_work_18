﻿#include <iostream>
#include <string>

using namespace std;

class Animal 
{
public:
	virtual void Voice() {cout << "Animals voice: ";}
	virtual ~Animal() {};
};
class Dog : public Animal
{
public:
	void Voice() override {cout<<"WOF! WOF!  ";}
	~Dog() override {};
};
class Cat : public Animal
{
public:
	void Voice() override {cout << "Meow  ";}
	~Cat() override {};
};
class Raven : public Animal
{
public:
	void Voice() override {cout << "Karrr-Karrr  ";}
	~Raven() override {};
};

int main()
{
	Animal* animals[4] = { new Animal, new Dog, new Cat, new Raven };
	for (int i = 0; i < 4; i++) 
	{ animals[i]->Voice(); 
	delete animals[i];
	}


}

